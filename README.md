meta-feabhas
============

Feabhas specific layer for the beaglebone to get it building. We use the 3.8 from https://github.com/beagleboard/kernel/tree/3.8

Download and use the danny branch for both poky and meta-ti.

Use this as an outline for the changes you'll need to make and then issue a

``
$ bitbake -k feabhas-image
``

Voila :D
