DESCRIPTION = "A small image just capable of allowing a device to boot."

IMAGE_INSTALL = "packagegroup-core-boot ${ROOTFS_PKGMANAGE_BOOTSTRAP} ${CORE_IMAGE_EXTRA_INSTALL} dropbear strace kmod e2fsprogs-mke2fs diffutils"

IMAGE_LINGUAS = " "

LICENSE = "MIT"

inherit core-image

IMAGE_ROOTFS_SIZE = "8192"

# Don't remove ipkg informations
# ROOTFS_POSTPROCESS_COMMAND += "remove_packaging_data_files ; "
